create database Rodoviaria;

create table passageiro(
	idPassageiro int not null auto_increment,
	 nome varchar(50),
     genero varchar(20), 
     RG varchar(13), 
     CPF varchar(16), 
     endereco varchar(50), 
     email varchar(100),
	telefone varchar(100)
) 
