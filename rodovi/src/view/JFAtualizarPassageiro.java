package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JLabel;

public class JFAtualizarPassageiro extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtRg;
	private JTextField txtCpf;
	private JTextField txtEndereco;
	private JTextField txtEmail;
	private JTextField txtTelefone;
	private static int id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFAtualizarPassageiro frame = new JFAtualizarPassageiro(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFAtualizarPassageiro(int id) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 877, 594);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		PassageiroDAO pdao = new PassageiroDAO();
		Passageiro p = pdao.read(id);
		
		JLabel lblIdp = new JLabel("ID:");
		lblIdp.setBounds(743, 21, 45, 13);
		contentPane.add(lblIdp);
		
		JLabel lblID = new JLabel("New label");
		lblID.setBounds(781, 21, 45, 13);
		contentPane.add(lblID);
		
		JLabel lblNewLabel_2 = new JLabel("Atualizar Passageiro");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_2.setBounds(10, 10, 199, 31);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_1 = new JLabel("Nome");
		lblNewLabel_1.setBounds(10, 61, 45, 13);
		contentPane.add(lblNewLabel_1);
		
		txtNome = new JTextField();
		txtNome.setBounds(10, 79, 266, 31);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblNewLabel_1_1 = new JLabel("G\u00EAnero");
		lblNewLabel_1_1.setBounds(10, 120, 45, 13);
		contentPane.add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("RG");
		lblNewLabel_1_1_1.setBounds(10, 185, 45, 13);
		contentPane.add(lblNewLabel_1_1_1);
		
		JLabel lblNewLabel_1_1_1_1 = new JLabel("CPF");
		lblNewLabel_1_1_1_1.setBounds(10, 249, 45, 13);
		contentPane.add(lblNewLabel_1_1_1_1);
		
		JLabel lblNewLabel_1_1_1_2 = new JLabel("Endere\u00E7o");
		lblNewLabel_1_1_1_2.setBounds(10, 455, 173, 13);
		contentPane.add(lblNewLabel_1_1_1_2);
		
		txtRg = new JTextField();
		txtRg.setColumns(10);
		txtRg.setBounds(10, 208, 266, 31);
		contentPane.add(txtRg);
		
		txtCpf = new JTextField();
		txtCpf.setColumns(10);
		txtCpf.setBounds(10, 272, 266, 31);
		contentPane.add(txtCpf);
		
		txtEndereco = new JTextField();
		txtEndereco.setColumns(10);
		txtEndereco.setBounds(10, 478, 266, 31);
		contentPane.add(txtEndereco);
		
		JLabel lblNewLabel_1_1_1_2_1 = new JLabel("Email");
		lblNewLabel_1_1_1_2_1.setBounds(10, 380, 45, 13);
		contentPane.add(lblNewLabel_1_1_1_2_1);
		
		JLabel lblNewLabel_1_1_1_2_2 = new JLabel("Telefone");
		lblNewLabel_1_1_1_2_2.setBounds(10, 313, 112, 13);
		contentPane.add(lblNewLabel_1_1_1_2_2);
		
		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(10, 403, 266, 31);
		contentPane.add(txtEmail);
		
		txtTelefone = new JTextField();
		txtTelefone.setColumns(10);
		txtTelefone.setBounds(10, 336, 266, 31);
		contentPane.add(txtTelefone);
		
		JRadioButton rdMasculino = new JRadioButton("Masculio");
		rdMasculino.setBounds(20, 139, 103, 21);
		contentPane.add(rdMasculino);
		
		JRadioButton rdFeminino = new JRadioButton("Feminino");
		rdFeminino.setBounds(125, 139, 103, 21);
		contentPane.add(rdFeminino);
		
		JRadioButton rdOutro = new JRadioButton("Outro");
		rdOutro.setBounds(230, 139, 103, 21);
		contentPane.add(rdOutro);
		
		ButtonGroup genero = new ButtonGroup();
		genero.add(rdMasculino);
		genero.add(rdFeminino);
		genero.add(rdOutro);
		
		lblID.setText(String.valueOf(p.getIdPassageiro()));
		txtNome.setText(p.getNome());
		if(p.getGenero() == "Masculio"){
			rdMasculino.setSelected(true);
		}else if(p.getGenero() == "Feminino") {
			rdFeminino.setSelected(true);
		}else {
			rdOutro.setSelected(true);
		}
		txtRg.setText(p.getRG());
		txtCpf.setText(p.getCPF());
		txtEndereco.setText(p.getEndereco());
		txtEmail.setText(p.getEmail());
		txtTelefone.setText(p.getTelefone());
		
		
		
		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Passageiro p = new Passageiro();
				PassageiroDAO dao = new PassageiroDAO();
				
				p.setIdPassageiro(Integer.parseInt(lblID.getText()));
				p.setNome(txtNome.getText());
				
				if(rdMasculino.isSelected()) {
					p.setGenero("Masculino");
				}else if(rdFeminino.isSelected()) {
					p.setGenero("Feminino");
				}else if(rdOutro.isSelected()){
					p.setGenero("Outro");
				}
				
				p.setRG(txtRg.getText());
				p.setCPF(txtCpf.getText());
				p.setEndereco(txtEndereco.getText());
				p.setEmail(txtEmail.getText());
				p.setTelefone(txtTelefone.getText());
				
				dao.update(p);
			}
			
		});
		
		btnAlterar.setBounds(371, 245, 121, 31);
		contentPane.add(btnAlterar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNome.setText(null);
				genero.clearSelection();
				txtRg.setText(null);
				txtCpf.setText(null);
				txtEndereco.setText(null);
				txtEmail.setText(null);
				txtTelefone.setText(null);
			}
		});
		btnLimpar.setBounds(371, 309, 121, 31);
		contentPane.add(btnLimpar);
		
		JButton btnCacelar = new JButton("Cacelar");
		btnCacelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCacelar.setBounds(371, 376, 121, 31);
		contentPane.add(btnCacelar);
		
	}
}
