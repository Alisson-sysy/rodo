package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class JFLogin extends JFrame {

	private JPanel contentPane;
	private JTextField txtLogin;
	private JPasswordField txtPass;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFLogin frame = new JFLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFLogin() {
		setTitle("SisRodoviaria - Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel txtWel = new JLabel("Welcome to RodoSis");
		txtWel.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		txtWel.setBounds(10, 10, 436, 25);
		contentPane.add(txtWel);
		
		JLabel txtSubWel = new JLabel("Inform your datas");
		txtSubWel.setFont(new Font("Tahoma", Font.BOLD, 10));
		txtSubWel.setBounds(10, 45, 228, 25);
		contentPane.add(txtSubWel);
		
		JLabel lblLogin = new JLabel("Login:");
		lblLogin.setBounds(10, 80, 45, 13);
		contentPane.add(lblLogin);
		
		txtLogin = new JTextField();
		txtLogin.setBounds(70, 77, 241, 19);
		contentPane.add(txtLogin);
		txtLogin.setColumns(10);
		
		JLabel lblPass = new JLabel("Password:");
		lblPass.setBounds(10, 117, 74, 13);
		contentPane.add(lblPass);
		
		txtPass = new JPasswordField();
		txtPass.setBounds(70, 114, 241, 19);
		contentPane.add(txtPass);
		
		JButton btnSign = new JButton("Sign in");
		btnSign.setBounds(10, 162, 85, 21);
		contentPane.add(btnSign);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBounds(105, 162, 85, 21);
		contentPane.add(btnCancel);
		
		JButton btnCreate = new JButton("Create an account ");
		btnCreate.setBounds(125, 232, 131, 21);
		contentPane.add(btnCreate);
		
		JButton btnForgot = new JButton("Forgot password");
		btnForgot.setBounds(200, 162, 131, 21);
		contentPane.add(btnForgot);
		 
		JLabel txtNew = new JLabel("New to RodoSisi?");
		txtNew.setFont(new Font("Tahoma", Font.BOLD, 13));
		txtNew.setBounds(10, 236, 131, 13);
		contentPane.add(txtNew);
	}
}
