package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFListarPassageiros extends JFrame {

	private JPanel contentPane;
	private JTable JfPassageiro;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFListarPassageiros frame = new JFListarPassageiros();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFListarPassageiros() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 806, 476);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Lista de Passageiros");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setBounds(37, 0, 294, 44);
		contentPane.add(lblNewLabel);
		
		JScrollPane scroll = new JScrollPane();
		scroll.setBounds(37, 59, 699, 275);
		contentPane.add(scroll);
		
		JfPassageiro = new JTable();
		JfPassageiro.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null},
			},
			new String[] {
				"Mypenes", "nome", "genero", "RG", "CPF", "endereco", "email", "telefone"
			}
		));
		scroll.setViewportView(JfPassageiro);
		
		JButton btnNewButton = new JButton("Cadastrar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFCadastrarPassageiro cp = new JFCadastrarPassageiro();
				cp.setVisible(true);
			}
		});
		btnNewButton.setBounds(35, 362, 117, 34);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Editar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//verificar linhha selecionada
				if(JfPassageiro.getSelectedRow() != -1) {
					JFAtualizarPassageiro ap = new JFAtualizarPassageiro((int) JfPassageiro.getValueAt(JfPassageiro.getSelectedRow(), 0));
					ap.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null, "Selecione um Passageiro");
				}
				readJTable();
			}
		});
		btnNewButton_1.setBounds(162, 362, 106, 34);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Excluir");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(JfPassageiro.getSelectedRow() != -1) {
					int opcao = JOptionPane.showConfirmDialog(null, "Deseja excluir esse passageiro", "Exclus�o", JOptionPane.YES_NO_OPTION);
					if(opcao == 0) {
						PassageiroDAO dao = new PassageiroDAO();
						Passageiro p = new Passageiro();
						p.setIdPassageiro((int)JfPassageiro.getValueAt(JfPassageiro.getSelectedRow(), 0));
						dao.delete(p);
					}
				}else {
					JOptionPane.showMessageDialog(null, "Selecione um filme");
				}
				readJTable();
			}
		});
		btnNewButton_2.setBounds(278, 362, 117, 34);
		contentPane.add(btnNewButton_2);
		
		readJTable();
	}
	
	public void readJTable() {
		DefaultTableModel modelo = (DefaultTableModel) JfPassageiro.getModel();
		modelo.setNumRows(0);
		
		PassageiroDAO pdao = new PassageiroDAO();
		for(Passageiro p: pdao.read()) {
			modelo.addRow(new Object[] {
					p.getIdPassageiro(),
					p.getNome(),
					p.getGenero(),
					p.getRG(),
					p.getCPF(),
					p.getEndereco(),
					p.getEmail(),
					p.getTelefone()
			});
		}
	} 
}
